import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import "./index.css";
import LoggedInLayout from "./components/layout/LoggedInLayout";
import LoggedOutLayout from "./components/layout/LoggedOutLayout";
import LoginForm from "./components/LoginForm";
import RegistrationForm from "./components/RegistrationForm";
import Landing from "./routes/Landing";
import CreateNeed from "./routes/CreateNeed";
import NeedsDetail from "./routes/NeedsDetail";
import NeedsList from "./routes/NeedsList";
import {Provider} from "react-redux";
import {store} from "./store";
import {getAllNeeds, getItemsFromCms, getNeedsDetail} from "./api";
import {userActions} from "./store/users/users.slice";

const router = createBrowserRouter([
  {
    path: "/",
    element: <LoggedInLayout />,
    children: [
      {index: true, element: <Landing /> },
      {path: '/needs/create', element: <CreateNeed />,
        loader: async () => {
        return await getItemsFromCms();
        }
      },
      {
        path: '/needs/:needId',
        element: <NeedsDetail />,
        loader: async ({params}) => {
          const res = await getNeedsDetail(`${params.needId}`)
          return res;
    }
      },
      {
        path: '/needs',
        element: <NeedsList />,
        loader: async () => {
          return await getAllNeeds()
        }
    },
    ]
  },
  {
    path:'/auth',
    element: <LoggedOutLayout />,
    children: [
      {path: '/auth/login', element: <LoginForm />},
      {path: '/auth/registration', element: <RegistrationForm />}
    ]
  }
]);

// Load token from local storage
const token = localStorage.getItem('token');
if(token){
  store.dispatch(userActions.setUser(token));
}

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <Provider store={store}>
      <RouterProvider router={router} />
    </Provider>
  </React.StrictMode>,
)
