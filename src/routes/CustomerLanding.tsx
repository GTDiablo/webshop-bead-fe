import {memo} from 'react';
import {Box, Divider, Typography} from "@mui/material";
import LandingNeedsList from "../components/LandingNeedsList";
import {useSelector} from "react-redux";
import {selectMyCompletedNeeds, selectMyNeeds} from "../store/users/users.selectors";

const CustomerLanding = () => {
    const myNeeds = useSelector(selectMyNeeds);
    const myCompletedNeeds = useSelector(selectMyCompletedNeeds);

    return (
        <Box>
            <Typography variant="h3">Megrendeléseim:</Typography>
            <LandingNeedsList needs={myNeeds} />
            <Divider sx={{my: 3}} />
            <Typography variant="h3">Teljesített megrendeléseim:</Typography>
            <LandingNeedsList needs={myCompletedNeeds} />
        </Box>
    );
}

export default memo(CustomerLanding);