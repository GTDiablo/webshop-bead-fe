import {memo} from 'react';
import {Box, Divider, Typography} from "@mui/material";
import LandingNeedsList from "../components/LandingNeedsList";
import {useSelector} from "react-redux";
import {
    selectMyAcceptedOffers,
    selectMyOffers
} from "../store/users/users.selectors";

const ProviderLanding = () => {
    const myOffers = useSelector(selectMyOffers);
    const myAcceptedOffers = useSelector(selectMyAcceptedOffers);
    return (
        <Box>
            <Typography variant="h3">Ajánlataim:</Typography>
            <LandingNeedsList needs={myOffers} />
            <Divider sx={{my: 3}} />
            <Typography variant="h3">Elfogadott ajánlataim:</Typography>
            <LandingNeedsList needs={myAcceptedOffers} />
        </Box>
    );
}

export default memo(ProviderLanding);