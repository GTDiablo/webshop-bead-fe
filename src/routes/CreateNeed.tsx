// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import React, {useEffect, useState} from 'react';
import {Box, Grid, Typography, Stack, Button} from "@mui/material";
import {getItemsFromCms, makeNeed} from "../api";
import {useLoaderData, useNavigate} from "react-router-dom";

function CreateNeed(props) {
    const items = useLoaderData();
    const [cart, setCart ] = useState({});

    const onIncrease = (name) => {
        setCart(prevCart => {
            const newCart = {...prevCart};

            if(!newCart[name]){
                newCart[name] = 0
            }

            newCart[name] += 1;
            return newCart;
        })
    }

    const getProductName = (itemId) => {
        return items.find((item) => itemId === item.id).name ?? ''
    }

    const onDecrease = (id) => {
        setCart(prevCart => {
            const newCart = {...prevCart};

            if(!newCart[id]){
                newCart[id] = 0
                return newCart;
            }

            newCart[id] -= 1;
            return newCart;
        })
    }

    const navigate = useNavigate();
    const onSubmit = async () => {
        const data = Object.entries(cart).map(([name, amount]) => ({
            name,
            amount
        }))
        const need = await makeNeed(data);
        navigate(`/needs/${need._id}`);
    };

    return (
        <Box>
            <Typography variant="h4" mb={2}>Megrendelés kiállítás</Typography>
            <Grid container spacing={2}>
                <Grid item xs={8}>
                    {items.map(({name}) => (
                        <Stack direction="row" key={name} justifyContent="space-between" sx={{my: 2, borderBottom: '1px solid black'}}>
                            <Typography>
                                {name}
                            </Typography>
                            <Box>
                                <button onClick={() => onDecrease(name)}>-</button>
                                {cart[name] ?? 0}
                                <button onClick={() => onIncrease(name)}>+</button>
                            </Box>
                        </Stack>
                    ))}
                </Grid>
                <Grid item xs={4}>
                    <Button onClick={onSubmit}>Küldés</Button>
                    <Box>
                        {Object.entries(cart).map(([name, amount]) => {
                            if(amount > 0){
                                return (
                                    <Stack key={name} direction="row" justifyContent="space-between">
                                <Typography>{name}</Typography>
                                <Typography>x{amount}</Typography>
                            </Stack>
                                )
                            }
                        })}
                    </Box>
                </Grid>
            </Grid>
        </Box>
    );
}

export default CreateNeed;