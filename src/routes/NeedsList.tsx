// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import React from 'react';
import {Box, Card, CardContent, Stack, Typography} from "@mui/material";
import {Link, useLoaderData} from "react-router-dom";

function NeedsList(props) {
    const data = useLoaderData();
    return (
        <Box>
            <Typography variant="h3">Megrendelések</Typography>
            <Stack>
                {data.map(({_id, createdAt, author, items}) => (
                    <Link key={_id} to={`/needs/${_id}`}>
                        <Card  variant="outlined">
                        <CardContent>
                            <Box>
                        <Stack>
                            <Typography>{_id}</Typography>
                            <Typography>{author.username}</Typography>
                            <Typography>{createdAt}</Typography>
                            <Typography>Mennyiség: {items.length}</Typography>
                        </Stack>
                    </Box>
                        </CardContent>
                    </Card>
                    </Link>
                ))}
            </Stack>
        </Box>
    );
}

export default NeedsList;