// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import React from 'react';
import CustomerLanding from "./CustomerLanding";
import ProviderLanding from "./ProviderLanding";
import {useSelector} from "react-redux";
import {selectIsProvider} from "../store/users/users.selectors";

function Landing(props) {
    const isCustomer = !useSelector(selectIsProvider);
    return (
        <div>
            {isCustomer && <CustomerLanding />}
            {!isCustomer && <ProviderLanding />}
        </div>
    );
}

export default Landing;