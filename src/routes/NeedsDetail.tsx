// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import React from 'react';
import {Box, Button, Chip, Divider, Stack, TextField, Typography} from "@mui/material";
import * as joi from "joi";
import {useForm} from "react-hook-form";
import {joiResolver} from "@hookform/resolvers/joi";
import {useLoaderData} from "react-router-dom";
import {acceptOffer, makeAnOffer} from "../api";
import {useSelector} from "react-redux";
import {selectIsProvider, selectUserId} from "../store/users/users.selectors";


function NeedsDetail(props) {
    const data = useLoaderData();

    const isUserProvider = useSelector(selectIsProvider);
    const userId = useSelector(selectUserId);
    const isMyNeed = data.author._id === userId;



    type FormValues = {
    price: number;
}

const schema = joi.object({
    price: joi.number().required(),
})

    const {register, handleSubmit, formState: {errors}} = useForm<FormValues>({
        resolver: joiResolver(schema)
    });

    const onSubmitOffer = handleSubmit((values) => {
        makeAnOffer(data._id, values.price);

    });

  const onAcceptOffer = (offerId: string) => {
      acceptOffer(data._id, offerId);
  }

    return (
        <Box>
            <Stack direction="row" justifyContent="space-between" mb="2">
              <Typography variant="h4" mb="2">Megrendelés #{data._id}</Typography>
              <Chip label={data.isOpen ? 'Nyitva' : 'Lezárt'} color={data.isOpen ? 'success' : 'warning'}/>
            </Stack><Stack direction="row" justifyContent="space-between" mb="2">
              <Typography mb="2">Megrendelő: {data.author.username}</Typography>
              <Typography  mb="2">Dátum: {data.createdAt}</Typography>
            </Stack>
          <Stack mt={5}>
            {data.items.map(({name, amount}) => (
                <Stack key={name} direction="row" spacing="2" justifyContent="space-between">
                  <Typography>{name}</Typography>
                  <Typography>x{amount}</Typography>
                </Stack>
            ))}
          </Stack>
          <Divider  />
          <Typography mb={2} variant="h6">Ajánlatok:</Typography>
          {(isUserProvider && data.isOpen) && (
              <Box my={3}>
                <Typography variant="h7">Adj meg egy ajánlatot:</Typography>
                 <Box component="form" onSubmit={onSubmitOffer} noValidate >
                   <TextField fullWidth inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }} error={!!errors.price?.message}  {...register('price')}/>
                   <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    sx={{ mt: 3, mb: 2 }}> Ajánlat elküldése</Button>
                 </Box>
              </Box>
          )}
          <Stack>
            {data.offers.map(({_id, author, isAccepted, price}) => (
                <Stack key={_id} direction="row" bgcolor={isAccepted ? 'lightgreen' : ''} spacing={1} justifyContent="space-between">
                  <Typography>{price} - {author.username} </Typography>
                  {(data.isOpen && !isUserProvider && isMyNeed ) && <Button onClick={() => onAcceptOffer(_id)}>Elfogadás</Button>}
                </Stack>
            ))}
          </Stack>
        </Box>
    );
}

export default NeedsDetail;