// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import axios from "axios";
import {redirect} from "react-router-dom";

const api = axios.create({
   baseURL: import.meta.env.VITE_APP_API_URL + '/api'
});

api.interceptors.request.use(
  config => {
    const token = localStorage.getItem('token');
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  },
  error => Promise.reject(error)
);
export const getMyData = async () => {
    try {
        const response = await api.get('/me');
        return response.data;
    } catch (e){
        return {};
    }

}

export const getNeedsDetail = async (needsId: string) => {
    try {
        const response = await api.get(`/needs/${needsId}`);
        return response.data;
    } catch (e){
        return {};
    }
}

export const getAllNeeds = async () => {
    try {
        const response = await api.get('/needs');
        return response.data;
    } catch (e){
        console.error(e);
        return [];
    }
}

export const apiRegister = async (username: string, password: string, isProvider: boolean) => {
    try{
        const response = await api.post('/register', {
           username,
           password,
           isProvider,
        });
        return await apiLogin(username, password);
    } catch (e){
        return false
    }
}

export const apiLogin = async (username: string, password: string) => {
    try {
        const repsone = await api.post('/login', {
            username,
            password
        });
        localStorage.setItem('token', repsone.data.token);
        redirect('/');
        return repsone.data.token;
    } catch (e){
        return null;
    }
}

export const makeAnOffer = async(needId: string, price: number) => {
    try {
        const response = await api.post(`/needs/${needId}/offers`, {price});
        return response.data;
    } catch (e){
        return {};
    }

}

export const makeNeed = async (items: any) => {
    try {
        const response = await api.post('/needs', {
           items
        });
        return response.data;
    } catch (e){
        return {}
    }
}

export const acceptOffer = async (needId:string, offerId: string ) => {
    try {
        const response = await api.post(`/needs/${needId}/offers/${offerId}/accept`);
        return response.data;
    } catch (e){
        return {};
    }
}

export const getItemsFromCms = async () => {
    try {
        const response = await axios.get(import.meta.env.VITE_APP_CMS_URL);
        const data = response.data.data;
        const formatted = data.map(o => ({
            name: o.attributes.name,
            id: o.id,
        }))
        return  formatted || [];
    } catch (e){
        return [];
    }
}