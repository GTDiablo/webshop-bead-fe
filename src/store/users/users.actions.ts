// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import {createAsyncThunk} from "@reduxjs/toolkit";

interface ILoginUser {
    username: string;
    password: string;
}

export const loginUser = createAsyncThunk(
    'users/loginUser',
    async (userDetail: ILoginUser, thunkAPI) => {

    }
);
//
// interface IRegisterUser {
//     username: string;
//     password: string;
//     type: 'customer' | 'provider';
// }
