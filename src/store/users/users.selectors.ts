import {createSelector} from "@reduxjs/toolkit";
import {USERS_KEY, UsersState} from "./users.slice";
import {RootState} from "../index";

const selectUsers = (state: RootState) => state[USERS_KEY];
export const selectIsProvider = createSelector(selectUsers, (state: UsersState) => {
    return state.isProvider;
});

export const selectUserName = createSelector(selectUsers, (state: UsersState) => {
    return state.username;
})

export const selectUserId = createSelector(selectUsers, (state: UsersState) => {
    return state.userId;
})
export const selectMyNeeds = createSelector(selectUsers, (state: UsersState) => {
   return state.myNeeds;
});
export const selectMyCompletedNeeds =createSelector(selectUsers, (state: UsersState) => {
    return state.myCompletedNeeds;
})
export const selectMyOffers = createSelector(selectUsers, (state: UsersState) => {
    return state.myOffers;
})
export const selectMyAcceptedOffers = createSelector(selectUsers, (state: UsersState) => {
    return state.myAcceptedOffers;
})