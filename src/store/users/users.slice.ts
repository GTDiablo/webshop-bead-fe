// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import jwtDecode from 'jwt-decode';


export const USERS_KEY = "users";

export interface UsersState {
    token: null | string;
    username: null | string;
    isProvider: null | boolean;
    userId: null | string;
    myNeeds: any[];
    myCompletedNeeds: any[];
    myOffers: any[];
    myAcceptedOffers: any[];
}

const initState: UsersState = {
    token: null,
    username: null,
    isProvider: null,
    userId: null,
    myNeeds: [],
    myCompletedNeeds: [],
    myOffers: [],
    myAcceptedOffers: [],
};

export const usersSlice = createSlice({
    name: USERS_KEY,
    initialState: initState,
    reducers: {
        setUser(state: UsersState, action: PayloadAction<string>){
            const {username, isProvider, userId } = jwtDecode(action.payload)
            state.username = username;
            state.isProvider = isProvider;
            state.userId = userId;
            state.token = action.payload;
            localStorage.setItem('token', action.payload);
        },
        logout(state) {
            state.username = null;
            state.token = null;
            state.isProvider = null;
            state.userId = null;
            localStorage.removeItem('token');
        },
        setMe(state: UsersState, action: PayloadAction<{
            myNeeds: any[],
            myCompletedNeeds: any[],
            myOffers: any[],
            myAcceptedOffers: any[],
        }>){
            state.myNeeds = action.payload.myNeeds
            state.myCompletedNeeds = action.payload.myCompletedNeeds
            state.myOffers = action.payload.myOffers
            state.myAcceptedOffers = action.payload.myAcceptedOffers
        }
    }
});

export const usersReducer = usersSlice.reducer;
export const userActions = usersSlice.actions;