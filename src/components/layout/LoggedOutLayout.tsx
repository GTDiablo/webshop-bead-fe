// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import React, {useEffect} from 'react';
import {Box} from "@mui/material";
import {Outlet, useNavigate} from "react-router-dom";
import {useSelector} from "react-redux";
import {selectUserName} from "../../store/users/users.selectors";

function LoggedOutLayout(props) {
    const userName = useSelector(selectUserName);
    const navigate = useNavigate();

    useEffect(()=> {
        if(userName){
            navigate('/');
        }
    }, [userName]);
    return (
        <Box sx={{width: '100vw', height: '100vh', display: 'flex', alignItems: 'center', justifyContent: 'center', background: 'white'}}>
            <Outlet />
        </Box>
    );
}

export default LoggedOutLayout;