// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import React from 'react';
import {Box, Card, CardContent, Stack, Typography} from "@mui/material";
import {Link} from "react-router-dom";

function LandingNeedsList(props) {
    return (
        <Box sx={{width: '100%'}}>
            <Stack direction="row" spacing={2}>
                {props.needs.map(({_id, items, created, author}) => (
                    <Link to={`/needs/${_id}`} key={_id}>
                        <Card>
                            <CardContent>
                                <Stack>
                                    <Typography>#{_id}</Typography>
                                    <Typography>{created}</Typography>
                                    <Typography>{author.username}</Typography>
                                    <Typography>Mennyiség: x{items.length}</Typography>
                                </Stack>
                            </CardContent>
                        </Card>
                    </Link>
                ))}
            </Stack>
        </Box>
    );
}

export default LandingNeedsList;