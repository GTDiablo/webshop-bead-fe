// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import React, {memo} from 'react';
import {Box, Container, Button, TextField, Avatar, FormControlLabel, Checkbox} from "@mui/material";
import {Controller, useForm} from "react-hook-form";
import * as joi from 'joi';
import {joiResolver} from "@hookform/resolvers/joi";
import Typography from "@mui/material/Typography";
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import {apiRegister} from "../api";
import {useNavigate} from "react-router-dom";

type FormValues = {
    username: string;
    password: string;
    isProvider: boolean;
}

const schema = joi.object({
    username: joi.string().required(),
    password: joi.string().required(),
    isProvider: joi.boolean().optional().default(false)
})
function RegistrationForm() {
    const {register, handleSubmit, formState: {errors}, control} = useForm<FormValues>({
        resolver: joiResolver(schema),
        defaultValues: {
            isProvider: false,
        }
    });

    const onSubmit = handleSubmit((data) => {
        apiRegister(data.username, data.password, data.isProvider);
    });

    const navigate = useNavigate();

    return (
        <Container component="main" maxWidth="xs">
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
            <LockOutlinedIcon />
          </Avatar>
            {JSON.stringify(errors)}
          <Typography component="h1" variant="h5">
            Regisztráció
          </Typography>
          <Box component="form" onSubmit={onSubmit} noValidate sx={{ mt: 1 }}>
            <TextField
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              autoComplete="email"
              autoFocus
              error={!!errors.username?.message}  variant="standard" {...register('username')}
            />
            <TextField
              margin="normal"
              required
              fullWidth
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
               error={!!errors.password?.message} variant="standard" {...register('password')}
            />
              <FormControlLabel color="black" sx={{color: 'black'}}
              control={
                  <Controller
                      control={control}
                      render={({field: props}) =>(
                          <Checkbox
                              {...props}
                              checked={props.value}
                              color="primary"
                              onChange={(e) => props.onChange(e.target.checked)}
                          />
                      )}
                      name='isProvider'
                  />
              }
              label="Kereskedőként regisztrálok"
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Regisztrálás
            </Button>
               <Button
              fullWidth
              variant="outlined"
              sx={{ mt: 3, mb: 2 }}
              onClick={() => navigate('/auth/login')}
            >
              Bejelentkezés
            </Button>
          </Box>
        </Box>
      </Container>
    );
}

export default memo(RegistrationForm);