// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import React, {memo} from 'react';
import {Box, Container, Button, TextField, Avatar} from "@mui/material";
import {useForm} from "react-hook-form";
import * as joi from 'joi';
import {joiResolver} from "@hookform/resolvers/joi";
import Typography from "@mui/material/Typography";
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import {apiLogin, apiRegister} from "../api";
import {useDispatch} from "react-redux";
import {useNavigate} from "react-router-dom";
import {userActions, usersSlice} from "../store/users/users.slice";

type FormValues = {
    username: string;
    password: string;
}

const schema = joi.object({
    username: joi.string().required(),
    password: joi.string().required(),
})
function LoginForm() {
    const {register, handleSubmit, formState: {errors}} = useForm<FormValues>({
        resolver: joiResolver(schema)
    });

    const dispatch = useDispatch();
    const navigate = useNavigate();

    const onSubmit = handleSubmit(async (data) => {
      const token = await apiLogin(data.username, data.password);

      if(!token){
          return
      }
      dispatch(userActions.setUser(token));
      navigate('/');
    });

    return (
        <Container component="main" maxWidth="xs">
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <Box component="form" onSubmit={onSubmit} noValidate sx={{ mt: 1 }}>
            <TextField
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              autoComplete="email"
              autoFocus
              error={!!errors.username?.message}  variant="standard" {...register('username')}
            />
            <TextField
              margin="normal"
              required
              fullWidth
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
               error={!!errors.password?.message} variant="standard" {...register('password')}
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Bejelentkezés
            </Button>
              <Button
              fullWidth
              variant="outlined"
              sx={{ mt: 3, mb: 2 }}
              onClick={() => navigate('/auth/registration')}
            >
              Regisztráció
            </Button>
          </Box>
        </Box>
      </Container>
    );
}

export default memo(LoginForm);